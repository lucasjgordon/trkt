//
//  NetworkError.swift
//  trkt
//
//  Created by Lucas Gordon on 09/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

enum NetworkError: Error {

    case internalError
    case invalidURL
    case noConnection
    case requestFailed

}
