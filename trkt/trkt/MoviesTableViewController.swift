//
//  MoviesTableViewController.swift
//  trkt
//
//  Created by Lucas Gordon on 09/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import UIKit

class MoviesTableViewController: UITableViewController {

    let movieService = MovieService()
    
    var movies: [Movie] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        movieService.getTrending { (movies, error) in
            if let error = error {
                switch error {
                case .internalError:
                    self.displayAlert("Something went wrong!", message: "Please try again later")
                case .invalidURL:
                    self.displayAlert("Something went wrong!", message: "Please try again later")
                case .noConnection:
                    self.displayAlert("Network error", message: "You don't seem to have a network connection at the moment, please try again later!")
                case .requestFailed:
                    self.displayAlert("Something went wrong!", message: "Please try again later")
                }
                return
            }

            self.movies = movies
        }
    }
    
}

// MARK: Private Methods
private extension MoviesTableViewController {
    
    func displayAlert(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: Table View Methods
extension MoviesTableViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? MovieTableViewCell else {
            return UITableViewCell()
        }
        
        let movie = self.movies[indexPath.row]
        
        if let imageString = movie.image, let image = UIImage(named: imageString) {
            cell.movieImageView.image = image
        }

        cell.movieTitleLabel.text = movie.title
        cell.movieYearLabel.text = String(movie.year)
        
        return cell
    }

}

// MARK: Navigation
extension MoviesTableViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            let cell = sender as? MovieTableViewCell,
            let indexPath = tableView.indexPath(for: cell),
            let vc = segue.destination as? MovieDetailViewController
        else {
            return
        }

        vc.movie = self.movies[indexPath.row]
    }

}
