//
//  NetworkService.swift
//  trkt
//
//  Created by Lucas Gordon on 09/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import Alamofire

struct NetworkService {
    
    let apiUrl: String
    let headers: HTTPHeaders
    
    init(apiUrl: String, headers: HTTPHeaders) {
        self.apiUrl = apiUrl
        self.headers = headers
    }
    
    func get(_ path: String, completionHandler: @escaping (_ response: Response) -> Void) {
        let networkReachabilityManager = NetworkReachabilityManager()!
        
        if !networkReachabilityManager.isReachable {
            completionHandler(.error(error: .noConnection))
            return
        }
        
        let urlString = "\(self.apiUrl)\(path)"
        
        guard let url = URL(string: urlString) else {
            completionHandler(.error(error: .invalidURL))
            return
        }
        
        Alamofire.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { response in
            guard let status = response.response?.statusCode else {
                completionHandler(.error(error: .requestFailed))
                return
            }

            switch(status){
            case 200:
                guard let result = response.result.value else {
                    completionHandler(.error(error: .requestFailed))
                    return
                }
                completionHandler(.success(response: result))
            default:
                completionHandler(.error(error: .requestFailed))
            }
        }
    }
    
}
