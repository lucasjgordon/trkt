//
//  MovieService.swift
//  trkt
//
//  Created by Lucas Gordon on 09/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import CoreData
import UIKit

struct MovieService: Fetchable {
    
    let context: NSManagedObjectContext
    
    let traktService = NetworkService(apiUrl: "https://api.trakt.tv/movies/", headers: [
        "Content-type": "application/json",
        "trakt-api-key": "0e7e55d561c7e688868a5ea7d2c82b17e59fde95fbc2437e809b1449850d4162",
        "trakt-api-version": "2"
    ])
    
    init() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.context = appDelegate.persistentContainer.newBackgroundContext()
    }
    
    internal func fetch(_ path: String, page: Int?, limit: Int?, completionHandler: @escaping ([Movie], NetworkError?) -> Void) {
        var urlString = "\(path)?"
        
        if let page = page {
            urlString = "\(urlString)page=\(page)&"
        }
        
        if let limit = limit {
            urlString = "\(urlString)limit=\(limit)&"
        }
        
        self.traktService.get(urlString) { response in
            switch response {
            case .success(let response):
                guard let items = response as? [[String: Any]] else {
                    completionHandler([], .requestFailed)
                    return
                }
                
                var movies: [Movie] = []
                
                let entity = NSEntityDescription.entity(forEntityName: "Movie", in: self.context)!
                
                for item in items {
                    guard
                        let movieData = item["movie"] as? [String: Any],
                        let title = movieData["title"] as? String,
                        let year = movieData["year"] as? Int,
                        let ids = movieData["ids"] as? [String: Any],
                        let id = ids["slug"] as? String
                    else {
                        continue
                    }
                    
                    let fetch = NSFetchRequest<NSFetchRequestResult>()
                    fetch.entity = entity
                    fetch.predicate = NSPredicate(format: "id == %@", id)
                    
                    do {
                        let fetchedObjects = try self.context.fetch(fetch)
                        if fetchedObjects.count > 0 {
                            movies.append(fetchedObjects.first as! Movie)
                            continue
                        }
                    } catch {
                        completionHandler([], .internalError)
                        return
                    }
                    
                    let movie = NSManagedObject(entity: entity, insertInto: self.context) as! Movie
                    
                    movie.setValue(title, forKeyPath: "title")
                    movie.setValue(year, forKeyPath: "year")
                    movie.setValue(id, forKeyPath: "id")
                    
                    // TODO image
                    
                    movies.append(movie)
                }
                
                completionHandler(movies, nil)
                
                do {
                    try self.context.save()
                } catch {
                    completionHandler([], .internalError)
                    return
                }
            case .error(let error):
                completionHandler([], error)
            }
        }
    }

    func getTrending(completionHandler: @escaping ([Movie], NetworkError?) -> Void) {
        self.fetch("trending", page: 1, limit: 20, completionHandler: completionHandler)
    }
    
    func getMovie(by id: String, completionHandler: @escaping (Movie?, NetworkError?) -> Void) {
        guard let entity = NSEntityDescription.entity(forEntityName: "Movie", in: self.context) else {
            completionHandler(nil, .internalError)
            return
        }

        let fetch = NSFetchRequest<NSFetchRequestResult>()
        fetch.entity = entity
        fetch.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let fetchedObjects = try self.context.fetch(fetch)
            if fetchedObjects.count == 0 {
                completionHandler(nil, .internalError)
                return
            }
            guard let movie = fetchedObjects[0] as? Movie else {
                completionHandler(nil, .internalError)
                return
            }
            
            let currentDate = NSDate()
            
            if let dateModified = movie.dateModified, currentDate.timeIntervalSince(dateModified as Date) < 600 {
                completionHandler(movie, nil)
                return
            }

            self.traktService.get("\(id)?extended=full") { response in
                switch response {
                case .success(let response):
                    guard
                        let item = response as? [String: Any],
                        let rating = item["rating"] as? Int,
                        let overview = item["overview"] as? String,
                        let genres = item["genres"] as? [String] // TODO add genres to movie
                    else {
                        return
                    }

                    movie.rating = Int16(rating)
                    movie.overview = overview
                    movie.dateModified = Date() as NSDate?

                    completionHandler(movie, nil)

                    do {
                        try self.context.save()
                    } catch {
                        completionHandler(nil, .internalError)
                        return
                    }
                case .error(let error):
                    completionHandler(nil, error)
                }
            }
        } catch {
            completionHandler(nil, .internalError)
        }
    }

}
