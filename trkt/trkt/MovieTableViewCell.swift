//
//  MovieTableViewCell.swift
//  trkt
//
//  Created by Lucas Gordon on 09/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    @IBOutlet var movieImageView: UIImageView!
    @IBOutlet var movieTitleLabel: UILabel!
    @IBOutlet var movieYearLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.movieImageView.layer.cornerRadius = 4
    }

}
