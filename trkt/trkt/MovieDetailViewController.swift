//
//  MovieDetailViewController.swift
//  trkt
//
//  Created by Lucas Gordon on 09/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
    
    @IBOutlet var movieImageView: UIImageView!
    @IBOutlet var movieTitleLabel: UILabel!
    @IBOutlet var movieYearLabel: UILabel!
    @IBOutlet var movieRatingLabel: UILabel!
    @IBOutlet var movieOverviewLabel: UILabel!
    
    let movieService = MovieService()

    var movie: Movie?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.movieTitleLabel.text = nil
        self.movieYearLabel.text = nil
        self.movieRatingLabel.text = nil
        self.movieOverviewLabel.text = nil
        
        guard
            let movie = movie,
            let id = movie.id
        else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        self.movieService.getMovie(by: id) { (movie, error) in
            if error != nil {
                self.dismiss(animated: true, completion: nil)
                return
            }
            
            self.movie = movie
            
            if let imageString = movie?.image, let image = UIImage(named: imageString) {
                self.movieImageView.image = image
            }
            
            self.movieTitleLabel.text = movie?.title
            
            if let year = movie?.year {
                self.movieYearLabel.text = String(year)
            }

            if let rating = movie?.rating {
                self.movieRatingLabel.text = "\(String(rating))/10"
            }

            self.movieOverviewLabel.text = movie?.overview
        }
    }

}
