//
//  Fetchable.swift
//  trkt
//
//  Created by Lucas Gordon on 09/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

protocol Fetchable {
    
    associatedtype T
    
    func fetch(_ path: String, page: Int?, limit: Int?, completionHandler: @escaping ([T], NetworkError?) -> Void)
    
}
